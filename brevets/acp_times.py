"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow
import math

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#

BREVETS = [[0.0, 200.0, 15.0, 34.0], [200.0, 400.0, 15.0, 32.0], [400.0, 600.0, 15.0, 30.0],
           [600.0, 1000.0, 11.428, 28.0]]

BREVET_STOP = [[200, 13.5], [300, 20], [400, 27], [600, 40], [1000, 75]]


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):

    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    print("brevet_start_time = " + brevet_start_time)
    start_time = arrow.get(brevet_start_time, 'YYYY-MM-DDTHH:mm')
    print(start_time)
    shift_min = 0
    total = 0
    passed_brevets = 0
    if type(control_dist_km) == float:
        control_dist_km = round(control_dist_km)

    for item in BREVETS:
        if item[0] <= control_dist_km <= item[1]:
            # Account for if the distance equals the brevet
            if control_dist_km == item[0] or control_dist_km == item[1]:
                shift_min = control_dist_km - (passed_brevets * 200)
            # Else, set it back to itself modulo 200 (for other brevets)
            else:
                if passed_brevets == 3:
                    shift_min = control_dist_km % 600
                else:
                    shift_min = control_dist_km % 200
            # Let's get that extra time
            shift_min = shift_min/item[3]
            if len(str(shift_min)) > 3:
                shift_min = float("%.2f" % shift_min)
            break
        else:
            total += 200 / item[3]
            passed_brevets += 1

    # Account for the other brevets
    shift_min += total

    if shift_min != 0:
        new_hours = int(shift_min)
        new_minutes = round((shift_min - new_hours) * 60)
        # print("OPENING:")
        # print("Minutes: " + str(new_minutes))
        # print("Hours: " + str(new_hours))
        ret_start_time = start_time.shift(hours=+new_hours, minutes=+new_minutes)
    else:
        ret_start_time = start_time.shift(hours=+0)

    print(ret_start_time.isoformat())
    return ret_start_time.isoformat()[:16]


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    print("brevet_start_time = " + brevet_start_time)
    end_time = arrow.get(brevet_start_time, 'YYYY-MM-DDTHH:mm')
    print(end_time)
    shift_max = 0
    total = 0
    passed_brevets = 0
    if type(control_dist_km) == float:
        control_dist_km = round(control_dist_km)

    # If this is the end, let's set up our special rule
    for item in BREVET_STOP:
        print(control_dist_km, int(brevet_dist_km), item[0])
        if control_dist_km == int(brevet_dist_km) == item[0]:
            print("yay")
            print(item[1])
            ret_end_time = end_time.shift(hours=+item[1])
            return ret_end_time.isoformat()[:16]

    for item in BREVETS:
        if item[0] <= control_dist_km <= item[1]:
            # Account for if the distance equals the brevet
            if control_dist_km == item[0] or control_dist_km == item[1]:
                shift_max = control_dist_km - (passed_brevets * 200)
            # Else, set it back to itself modulo 200 (for other brevets)
            else:
                if passed_brevets == 3:
                    shift_max = control_dist_km % 600
                else:
                    shift_max = control_dist_km % 200
            # Let's get that extra time
            shift_max = shift_max / item[2]
            if len(str(shift_max)) > 3:
                shift_max = float("%.2f" % shift_max)
            break
        else:
            total += 200 / item[2]
            passed_brevets += 1

        # Account for the other brevets
    shift_max += total

    if shift_max != 0:
        new_hours = int(shift_max)
        new_minutes = round((shift_max - new_hours) * 60)
        # print("CLOSING:")
        # print("Minutes: " + str(new_minutes))
        # print("Hours: " + str(new_hours))
        ret_end_time = end_time.shift(hours=+new_hours, minutes=+new_minutes)
    else:
        ret_end_time = end_time.shift(hours=+1)

    return ret_end_time.isoformat()[:16]
