import acp_times
import nose

def test_basic_functionality1():
    """Test between range 0 - 200"""
    print("Open_time: " + str(acp_times.open_time(50, 200, "2018-11-05T00:00")))
    print("Close_time: " + str(acp_times.close_time(50, 200, "2018-11-05T00:00")))
    assert str(acp_times.open_time(50, 200, "2018-11-05T00:00")) \
           == "2018-11-05T01:28"
    assert str(acp_times.close_time(50, 200, "2018-11-05T00:00")) \
           == "2018-11-05T03:20"

def test_basic_functionality2():
    """Test special value 200"""
    print("Open_time: " + str(acp_times.open_time(200, 200, "2018-11-05T00:00")))
    print("Close_time: " + str(acp_times.close_time(200, 200, "2018-11-05T00:00")))
    assert str(acp_times.open_time(200, 200, "2018-11-05T00:00")) \
           == "2018-11-05T05:53"
    assert str(acp_times.close_time(200, 200, "2018-11-05T00:00")) \
           == "2018-11-05T13:30"

def test_basic_functionality3():
    """Test between range 200 - 400"""
    print("Open_time: " + str(acp_times.open_time(250, 400, "2018-11-05T00:00")))
    print("Close_time: " + str(acp_times.close_time(250, 400, "2018-11-05T00:00")))
    assert str(acp_times.open_time(250, 400, "2018-11-05T00:00")) \
           == "2018-11-05T07:27"
    assert str(acp_times.close_time(250, 400, "2018-11-05T00:00")) \
           == "2018-11-05T16:40"

def test_basic_functionality4():
    """Test special value 400"""
    print("Open_time: " + str(acp_times.open_time(400, 400, "2018-11-05T00:00")))
    print("Close_time: " + str(acp_times.close_time(400, 400, "2018-11-05T00:00")))
    assert str(acp_times.open_time(400, 400, "2018-11-05T00:00")) \
           == "2018-11-05T12:08"
    assert str(acp_times.close_time(400, 400, "2018-11-05T00:00")) \
           == "2018-11-06T03:00"

def test_basic_functionality5():
    """Test between range 400 - 600"""
    print("Open_time: " + str(acp_times.open_time(425, 600, "2018-11-05T00:00")))
    print("Close_time: " + str(acp_times.close_time(425, 600, "2018-11-05T00:00")))
    assert str(acp_times.open_time(425, 600, "2018-11-05T00:00")) \
           == "2018-11-05T12:58"
    assert str(acp_times.close_time(425, 600, "2018-11-05T00:00")) \
           == "2018-11-06T04:20"

def test_basic_functionality6():
    """Test special value 600"""
    print("Open_time: " + str(acp_times.open_time(600, 600, "2018-11-05T00:00")))
    print("Close_time: " + str(acp_times.close_time(600, 600, "2018-11-05T00:00")))
    assert str(acp_times.open_time(600, 600, "2018-11-05T00:00")) \
           == "2018-11-05T18:48"
    assert str(acp_times.close_time(600, 600, "2018-11-05T00:00")) \
           == "2018-11-06T16:00"

def test_basic_functionality7():
    """Test between 600 - 1000"""
    print("Open_time: " + str(acp_times.open_time(888, 1000, "2018-11-05T00:00")))
    print("Close_time: " + str(acp_times.close_time(888, 1000, "2018-11-05T00:00")))
    assert str(acp_times.open_time(888, 1000, "2018-11-05T00:00")) \
           == "2018-11-06T05:05"
    assert str(acp_times.close_time(888, 1000, "2018-11-05T00:00")) \
           == "2018-11-07T17:12"

def test_basic_functionality8():
    """Test special value 1000"""
    print("Open_time: " + str(acp_times.open_time(1000, 1000, "2018-11-05T00:00")))
    print("Close_time: " + str(acp_times.close_time(1000, 1000, "2018-11-05T00:00")))
    assert str(acp_times.open_time(1000, 1000, "2018-11-05T00:00")) \
           == "2018-11-06T09:05"
    assert str(acp_times.close_time(1000, 1000, "2018-11-05T00:00")) \
           == "2018-11-08T03:00"

def test_decimal_case():
    """Test decimal rounding (according to https://rusa.org/octime_acp.html)"""
    print("Open_time: " + str(acp_times.open_time(103.5, 200, "2018-11-05T00:00")))
    print("Close_time: " + str(acp_times.close_time(103.5, 200, "2018-11-05T00:00")))
    assert str(acp_times.open_time(103.5, 200, "2018-11-05T00:00")) \
           == "2018-11-05T03:04"
    assert str(acp_times.close_time(103.5, 200, "2018-11-05T00:00")) \
           == "2018-11-05T06:56"

def test_date_cases1():
    """Test different dates to further test functionality"""
    print("Open_time: " + str(acp_times.open_time(700, 1000, "2019-12-10T10:05")))
    print("Close_time: " + str(acp_times.close_time(700, 1000, "2019-12-10T10:05")))
    assert str(acp_times.open_time(700, 1000, "2019-12-10T10:05")) \
           == "2019-12-11T08:27"
    assert str(acp_times.close_time(700, 1000, "2019-12-10T10:05")) \
           == "2019-12-12T10:50"

def test_date_cases2():
    """Test really strange dates to further test functionality"""
    print("Open_time: " + str(acp_times.open_time(199, 200, "2020-01-29T23:59:00")))
    print("Close_time: " + str(acp_times.close_time(199, 200, "2020-01-29T23:59:00")))
    assert str(acp_times.open_time(199, 200, "2020-01-29T23:59:00")) \
           == "2020-01-30T05:50"
    assert str(acp_times.close_time(199, 200, "2020-01-29T23:59:00")) \
           == "2020-01-30T13:15"

def test_weird_case():
    """What about at 0? (according to https://rusa.org/octime_acp.html)"""
    print("Open_time: " + str(acp_times.open_time(0, 200, "2018-11-05T00:00:00")))
    print("Close_time: " + str(acp_times.close_time(0, 200, "2018-11-05T00:00:00")))
    assert str(acp_times.open_time(0, 200, "2018-11-05T00:00:00")) \
           == "2018-11-05T00:00"
    assert str(acp_times.close_time(0, 200, "2018-11-05T00:00:00")) \
           == "2018-11-05T01:00"
