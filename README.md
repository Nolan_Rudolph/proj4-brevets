Welcome to my project!  
This software focuses exclusively on calculating ACP brevet controle times for some duration of race.  
The logic behind the back-end development of the outputted times are base on https://rusa.org/pages/acp-brevet-control-times-calculator.  
  
Over many years, the average top speed of racers in such RUSA races have been calculated between certain intervals of a race:  
From 0km - 200km, we expect a top speed of 34km/hr, therefore a brevet point would open up at (distance between 0km - 200km)/34  
From 200km - 400km, we expect a top speed of 32km/hr, therefore a brevet point would open up at (distance between 200km - 400km)/32  
From 400km - 600km, we expect a top speed of 30km/hr, therefore a brevet point would open up at (distance between 400km - 600km)/30  
From 600km - 1000km, we expect a top speed of 28km/hr, therefore a brevet point would open up at (distance between 600km - 1000km)/28  
  
Similarly, the average bottom speed of racers in such RUSA races have been calculated between certain intervals of a race:  
From 0km - 200km, we expect a bottom speed of 15km/hr, therefore a brevet point would close at (distance between 0km - 200km)/15  
From 200km - 400km, we expect a bottom speed of 15km/hr, therefore a brevet point would close at (distance between 200km - 400km)/15  
From 400km - 600km, we expect a bottom speed of 11.428km/hr, therefore a brevet point would close at (distance between 400km - 600km)/11.428  
From 600km - 1000km, we expect a bottom speed of 13.333km/hr, therefore a brevet point would close at (distance between 600km - 1000km)/13.333  

What makes these calculations so difficult is how one must account for previous points passed and incorporate this into the new opening/closing time of some later brevet point.  

Usage:
1.) Make sure to specify the total distance this race will be via the drop down "Duration" button.  
2.) Let the first distance be 0km/0miles, the beginning of the race.  
3.) Feel free to add any brevet points from 0km/0miles to the total distance you specefied earlier.  
4.) End with your total distance as the last brevet stop.  
5.) Note the times, and have a fun race! 

Author: Nolan Rudolph  
Duck-ID: 951553198  
E-mail: ngr@uoregon.edu  
